export const Restaurants = [
    {
        "id": 1,
        "place": "Tampines",
        "timing": {
            "open": "7:00",
            "close": "20:00"
        },
        "description": "We provide authentic food",
        "rating": 4,
        "menu": {
            "items": [{
                    "name": "coke",
                    "qty": 20,
                    "category": "drinks",
                    "sizes": [
                        "small",
                        "large"
                    ]
                },
                {
                    "name": "pepsi",
                    "qty": 20,
                    "category": "drinks",
                    "sizes": [
                        "small",
                        "large"
                    ]
                },
                {
                    "name": "water",
                    "qty": 20,
                    "category": "drinks",
                    "sizes": [
                        "small",
                        "large"
                    ]
                },
                {
                    "name": "fries",
                    "qty": 20,
                    "category": "junk food",
                    "sizes": [
                        "small",
                        "large"
                    ]
                },
                {
                    "name": "pizza",
                    "qty": 20,
                    "category": "junk food",
                    "sizes": [
                        "small",
                        "large"
                    ]
                }
            ],
            "categories": [
                "drinks",
                "junk food"
            ]
        },
        "category": "vegetarian",
        "name": "McVeg",
        "position": {
            "lat": 1.349591,
            "lng": 103.95679
        },
        "title": 'McVeg',
        "clickable" : true,
        "icon" : 'http://www.developerdrive.com/wp-content/uploads/2013/08/ddrive.png',  
    },
    {
        "id": 2,
        "place": "Kallang",
        "timing": {
            "open": "9:00",
            "close": "23:00"
        },
        "description": "We provide authentic food",
        "rating": 3,
        "menu": {
            "items": [{
                    "name": "coke",
                    "qty": 20,
                    "category": "drinks",
                    "sizes": [
                        "small",
                        "large"
                    ]
                },
                {
                    "name": "pepsi",
                    "qty": 20,
                    "category": "drinks",
                    "sizes": [
                        "small",
                        "large"
                    ]
                },
                {
                    "name": "water",
                    "qty": 20,
                    "category": "drinks",
                    "sizes": [
                        "small",
                        "large"
                    ]
                },
                {
                    "name": "fries",
                    "qty": 20,
                    "category": "junk food",
                    "sizes": [
                        "small",
                        "large"
                    ]
                },
                {
                    "name": "pizza",
                    "qty": 20,
                    "category": "junk food",
                    "sizes": [
                        "small",
                        "large"
                    ]
                }
            ],
            "categories": [
                "drinks",
                "junk food"
            ]
        },
        "category": "noodles",
        "name": "Restaurant2",
        "position": {
            "lat": 1.349591,
            "lng": 103.85679
        },
        "title": 'Restaurant2',
        "clickable" : true,
    }
]



export const FavouriteRestaurants = [
    {
        "id": 1,
        "place": "Tampines",
        "timing": {
            "open": "7:00",
            "close": "16:00"
        },
        "category": "vegetarian",
        "description": "We provide authentic food.",
        "name": "McVeg",
        "notes":"",
        "icon" : 'http://www.developerdrive.com/wp-content/uploads/2013/08/ddrive.png', 
        "rating": 4, 
    },
    {
        "id": 2,
        "place": "Kallang",
        "timing": {
            "open": "9:00",
            "close": "23:00"
        },
        "category": "noodles",
        "name": "Restaurant2",
        "description": "We provide authentic food",
        "notes":"visited last on sunday",        
        "rating": 3, 
    },
    {
        "id": 3,
        "place": "Kallang",
        "timing": {
            "open": "9:00",
            "close": "23:00"
        },
        "category": "noodles",
        "name": "Restaurant2",
        "description": "We provide authentic food",
        "notes":"visited last on sunday",        
        "rating": 3, 
    },
    {
        "id": 4,
        "place": "Kallang",
        "timing": {
            "open": "9:00",
            "close": "23:00"
        },
        "category": "noodles",
        "name": "Restaurant2",
        "description": "We provide authentic food",
        "notes":"visited last on sunday",        
        "rating": 3, 
    },
    {
        "id": 5,
        "place": "Kallang",
        "timing": {
            "open": "9:00",
            "close": "23:00"
        },
        "category": "noodles",
        "name": "Restaurant2",
        "description": "We provide authentic food",
        "notes":"visited last on sunday",        
        "rating": 3, 
    },
    {
        "id": 6,
        "place": "Kallang",
        "timing": {
            "open": "9:00",
            "close": "23:00"
        },
        "category": "noodles",
        "name": "Restaurant2",
        "description": "We provide authentic food",
        "notes":"visited last on sunday",        
        "rating": 3, 
    },
    {
        "id": 7,
        "place": "Kallang",
        "timing": {
            "open": "9:00",
            "close": "23:00"
        },
        "category": "noodles",
        "name": "Restaurant2",
        "description": "We provide authentic food",
        "notes":"visited last on sunday",        
        "rating": 3, 
    },
    {
        "id": 8,
        "place": "Kallang",
        "timing": {
            "open": "9:00",
            "close": "23:00"
        },
        "category": "noodles",
        "name": "Restaurant2",
        "description": "We provide authentic food",
        "notes":"visited last on sunday",        
        "rating": 3, 
    }
]